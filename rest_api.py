from flask import Flask, jsonify, render_template

app=Flask(__name__)

from slang import slang

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/palabras")
def palabras():
    return jsonify({"Palabras": slang})

@app.route("/palabras/<string:palabra_slang>")
def palabra(palabra_slang):
    pal = [palabra for palabra in slang if palabra['palabra'] == palabra_slang]
    if (len(pal) > 0):
        return jsonify({"Palabra": pal[0]})
    else:
        return jsonify({"Mensaje":"La palabra no se encuentra"})

if __name__ == "__main__":
    app.run(debug=True)